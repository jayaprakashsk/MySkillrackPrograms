#include<stdio.h>
#include <stdlib.h>
#include<string.h>

int isPallindrome(char st[])
{
    int k=0,i=0,j=strlen(st)-1;
    char temp[1000];
    strcpy(temp,st);
    while(temp[k]!='\0')
    {
        if(temp[k]>='A'&&temp[k]<='Z')
        {
            temp[k]+=32;
        }
        k++;
    }
    while(i<j)
    {
        if(temp[i++]!=temp[j--])
        {
            return 1;
        }
    }
    return 0;
}
int main()
{
    char str[1000][1000],temp[1000];
    int i=0;
    while(scanf("%s",str[i++])>0)
    {
        if(isPallindrome(str[i-1]))
        {
            printf("%s ",str[i-1]);
        }
    }

}